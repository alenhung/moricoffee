<!DOCTYPE html>
<html lang="zh-TW">
  <head>
    <meta charset="utf-8">
    <title><?php echo SITE_NAME;?> </title>
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="keyword" content="網站關鍵字 ">
    <meta name="description" content="網站的描述">
    <meta name="author" content="<?php echo SITE_NAME;?>">
    <!--MOBILE FIRST-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <link rel="stylesheet" href="<?php echo SITE_ROOT;?>stylesheets/styles.css" rel="stylesheet" media="screen">
    <!--Facebook相關設定-->
      <!--連接的標題-->
    <meta property="og:title" content="Moricoffee.com"/>
      <!--連接的預覽圖片-->
    <meta property="og:image" content=""/>
    <!--連接的網站名稱-->
    <meta property="og:site_name" content="moricoffee"/>
    <!--連接的描述-->
    <meta property="og:description" content="描述內容。"/>
  </head>
  <body>