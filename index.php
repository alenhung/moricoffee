<?php
	//載入網站基本設定
 	require 'include/config.php';
  	//載入Template/tp_site_header.php
  	require 'view/tp_SiteHeader.php';
	require 'view/tp_header.php';
	require 'view/tp_siteNav.php';
	//require 'view/tp_topBlock.php'; 
?>
	<div id="siteBlock" class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12">
			<img src="<?php echo SITE_ROOT;?>img/coffeeIntro.jpg" alt="" class="img-responsive">
			</div>
		</div>
		<div class="row">
			<div id="indexContent">
				<div class="col-lg-12 col-md-12 graybakcground">
					<p><span class="title1">MAKE GREAT COFFEE & ESPRESSO AT HOME</span></p>
					<p>Making coffee that you enjoy drinking takes three things: the right equipment, good beans, and a little knowledge. We’ve put together an edited collection of the best espresso machines, coffee makers, and craft roasted coffee beans for your home or office. After all, you should love your coffee ritual.</p>
				</div>
				<div class="col-lg-6 col-md-6 graybakcground">
					<p><span class="title2">ESPRESSO MACHINES WORTH OWNING.</span></p>
					<p>We've tested many and narrowed it down to a manageable selection of the best espresso machines available.</p>
				</div>
				<div class="col-lg-6 col-md-6 graybakcground">
					<p><span class="title2">COFFEE MAKERS THAT LAST.</span></p>
					<p>Most coffee makers make bad coffee. We've found two that optimize brew time and temperature for rich, complex coffee.</p>
				</div>
			</div>	
		</div>
	</div>
<?php
	require 'view/tp_siteFooter.php';
?>